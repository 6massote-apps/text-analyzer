This project was build using AngularJS for frontend web-application and SpringBoot (Java) for API. The main porpose of this project is offer to user a way to count a number of word in a specific text.

The funcional requirements follow the use cases below:

```
As a user
  when I view the application
    then I see a form containing a text box to enter a body of text

  and when I submit the form with some text
    then I see a result containing the number of words in the text-area

  and when I submit the form with an empty text
    then I see a form error telling me that some text input is required

As an engineer
  when I look at your project
    then I should understand how to install and run it
```

`Ps: this application is not production ready`

### 1. How to run

We are using [Docker Compose](https://docs.docker.com/engine/installation/) to run the project. So, the only thing you need to run is Docker and Docker Compose. The Docker will create two containers called text_analyzer_api and text_analyzer_web.

Once docker has been installed, run the commands below to create both containers:

```bash
# Build, create and run containers
$ docker-compose up --build
```

Endpoints of both applications:
- text_analyzer_web:8000 (http://localhost:8000)
- text_analyzer_api:8080 (http://localhost:8080)

For `text_analyzer_api` we have an endpoint `[POST] /api/text/analyze` that receives: 
```json
{
	"text": "How to test the number of words."
}
```

To remove all containers and create form zero again, run the commands below:

```bash
$ docker-compose down --rmi all --remove-orphans
$ docker-compose up --build
```

### 2. Some references

- [Commit messages pattern - Commit Karma] (http://karma-runner.github.io/0.13/dev/git-commit-msg.html)
- [Microservice testing] (https://martinfowler.com/articles/microservice-testing/)

----
Gabriel Prado [e-mail](email:gabrielmassote@gmail.com).
