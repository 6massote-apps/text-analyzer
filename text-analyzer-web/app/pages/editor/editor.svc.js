angular.module('app.editor.svc', [])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/editor', {
            templateUrl: 'pages/editor/new.html',
            controller: 'EditorCtrl'
        });        
    }])
    .service('TextAnalyzerApi', function($http) {
        this.analyze = function(editorPayload, successCallback, failureCallback) {
            $http.post("http://localhost:8080/api/text/analyze", editorPayload)
            .then(function(response) {
                successCallback(response);
            }).catch(function(error) {
            if (failureCallback)
                failureCallback(error);
            })
        }
    })
