angular.module('app.editor.ctrl', ['ngRoute', 'app.editor.svc'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/editor', {
            templateUrl: 'pages/editor/new.html',
            controller: 'EditorCtrl'
        });        
    }])
    .controller('EditorCtrl', ['$scope', '$location', 'TextAnalyzerApi', function($scope, $location, TextAnalyzerApi) {
        $scope.analyze = function() {
            if ($scope.editor == undefined || $scope.editor.text == undefined || $scope.editor.text == "" || $scope.editor.text.trim() == "") {
                $scope.error="The Text field is required."
                $scope.failure=true
                $scope.success=false

                return;
            }

            TextAnalyzerApi.analyze(
                $scope.editor, 
                function(textAnalyzed) {
                    $scope.success=true
                    $scope.failure=false
                    $scope.wordCount=textAnalyzed.data.wordCount;
                    $scope.characterCount=textAnalyzed.data.characterCount;
                }, function(textAnalyzedError) {
                    $scope.failure=true
                    $scope.success=false

                    if (textAnalyzedError.status == -1) {
                        $scope.error="Unexpected erro :("
                    }
                    
                    if (textAnalyzedError.status == 400) {
                        $scope.error="The Text field is required."
                    }                    
                })
        }
    }]);
