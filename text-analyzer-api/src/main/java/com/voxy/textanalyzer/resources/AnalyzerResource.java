package com.voxy.textanalyzer.resources;

import com.voxy.textanalyzer.domain.TextAnalyzerService;
import com.voxy.textanalyzer.domain.TextSummary;
import com.voxy.textanalyzer.resources.textanalyzer.AnalyzeRequestAPI;
import com.voxy.textanalyzer.resources.textanalyzer.AnalyzerSummaryResponseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequestMapping("/analyze")
@RestController
public class AnalyzerResource {

    @Autowired
    private TextAnalyzerService textAnalyzerService;

    @RequestMapping(method = RequestMethod.POST)
    public AnalyzerSummaryResponseAPI create(@Valid @RequestBody final AnalyzeRequestAPI analyzeRequestAPI) {
        final TextSummary textSummary = textAnalyzerService.analyze(analyzeRequestAPI.getText());

        return AnalyzerSummaryResponseAPI.builder()
                .wordCount(textSummary.getWordCount())
                .characterCount(textSummary.getCharacterCount())
                .textOriginal(textSummary.getTextOriginal())
                .build();
    }

}
