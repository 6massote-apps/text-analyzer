package com.voxy.textanalyzer.resources.textanalyzer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class AnalyzeRequestAPI {

    @NotNull
    private String text;

}
