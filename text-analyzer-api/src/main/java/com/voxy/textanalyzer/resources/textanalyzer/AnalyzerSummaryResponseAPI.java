package com.voxy.textanalyzer.resources.textanalyzer;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AnalyzerSummaryResponseAPI {

    public int wordCount;
    public long characterCount;
    public String textOriginal;

}
