package com.voxy.textanalyzer.configs;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CORSConfig extends CorsFilter {

    public CORSConfig() {
        super(configurationSource());
    }

    private static UrlBasedCorsConfigurationSource configurationSource() {
        final CorsConfiguration configApi = new CorsConfiguration();
        configApi.setAllowCredentials(false);
        configApi.addAllowedOrigin("*");
        configApi.addAllowedHeader("*");
        configApi.setAllowedMethods(Arrays.asList("POST", "PUT", "PATCH", "GET", "DELETE", "OPTIONS"));
        configApi.setExposedHeaders(Arrays.asList("Authorization", "Content-Type"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/api/**", configApi);
        source.registerCorsConfiguration("/api/text/**", configApi);
        source.registerCorsConfiguration("/api/text/analyze", configApi);

        source.registerCorsConfiguration("/analyze", configApi);
        source.registerCorsConfiguration("analyze", configApi);

        return source;
    }
}
