package com.voxy.textanalyzer.domain;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class TextAnalyzerService {

    public TextSummary analyze(final String text) {
        if (text == null)
            return TextSummary.builder().build();

        String textToAnalyze = text.trim();
        if (StringUtils.isEmpty(textToAnalyze)) {
            return TextSummary.builder()
                    .textOriginal(text)
                    .build();
        }

        String textWithoutFinalPonctuation = textToAnalyze.replaceAll("[\\.\\,\\!\\?]+", "");
        int wordsCount = textWithoutFinalPonctuation.split("\\s+").length;

        return TextSummary.builder()
                .wordCount(wordsCount)
                .characterCount(text.length())
                .textOriginal(text)
                .build();
    }

}
