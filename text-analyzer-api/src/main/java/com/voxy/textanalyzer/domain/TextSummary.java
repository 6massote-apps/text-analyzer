package com.voxy.textanalyzer.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TextSummary {

    public int wordCount;
    public long characterCount;
    public String textOriginal;

}
