package com.voxy.textanalyzer.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TextAnalyzerServiceTest {

    private String textToAnalyze = "Sinan a.a.a or it? Maybe two and 1982 , @23 Unur's";

    @Test
    public void analyze() throws Exception {
        TextSummary textSummary = new TextAnalyzerService().analyze(textToAnalyze);

        assertThat(textSummary).isNotNull();
        assertThat(textSummary.getWordCount()).isEqualTo(10);
        assertThat(textSummary.getCharacterCount()).isEqualTo(50);
        assertThat(textSummary.getTextOriginal()).isEqualTo(textToAnalyze);
    }

    @Test
    public void analyzeEmpty() throws Exception {
        TextSummary textSummary = new TextAnalyzerService().analyze("    ");

        assertThat(textSummary).isNotNull();
        assertThat(textSummary.getWordCount()).isEqualTo(0);
        assertThat(textSummary.getCharacterCount()).isEqualTo(0);
        assertThat(textSummary.getTextOriginal()).isEqualTo("    ");

        textSummary = new TextAnalyzerService().analyze("");

        assertThat(textSummary).isNotNull();
        assertThat(textSummary.getWordCount()).isEqualTo(0);
        assertThat(textSummary.getCharacterCount()).isEqualTo(0);
        assertThat(textSummary.getTextOriginal()).isEqualTo("");
    }

    @Test
    public void analyzeNull() throws Exception {
        TextSummary textSummary = new TextAnalyzerService().analyze(null);

        assertThat(textSummary).isNotNull();
        assertThat(textSummary.getWordCount()).isEqualTo(0);
        assertThat(textSummary.getCharacterCount()).isEqualTo(0);
        assertThat(textSummary.getTextOriginal()).isEqualTo(null);
    }

}