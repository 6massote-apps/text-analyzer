package com.voxy.textanalyzer.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.voxy.textanalyzer.TextAnalyzerApplication;
import com.voxy.textanalyzer.resources.textanalyzer.AnalyzerSummaryResponseAPI;
import com.voxy.textanalyzer.resources.textanalyzer.AnalyzeRequestAPI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = TextAnalyzerApplication.class)
@AutoConfigureMockMvc
public class AnalyzerResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${test.text.specialchars}")
    private String textToSubmitted;

    @Test
    public void shouldReturnTextAnalyzed() throws Exception {
        AnalyzeRequestAPI analyzeRequestAPI = AnalyzeRequestAPI.builder()
                .text(textToSubmitted)
                .build();

        AnalyzerSummaryResponseAPI analyzerSummaryResponseAPI = AnalyzerSummaryResponseAPI.builder()
                .wordCount(10)
                .characterCount(50)
                .textOriginal(textToSubmitted)
                .build();

        mockMvc.perform(post("/analyze")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(analyzeRequestAPI)))
                .andExpect(content().string(equalTo(objectMapper.writeValueAsString(analyzerSummaryResponseAPI))))
                .andExpect(status().is2xxSuccessful());
    }

}